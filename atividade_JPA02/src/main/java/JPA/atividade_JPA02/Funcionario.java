package JPA.atividade_JPA02;

import java.util.Date;
import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name = "tab_funcionarios", uniqueConstraints = @UniqueConstraint(name = "uk_fun_rg", columnNames = { "fun_rg",
		"fun_rg_orgao_expedidor", "fun_rg_uf" }))
public class Funcionario {

	@Id
	@Column(name = "fun_cpf", columnDefinition = "char(11)")
	private String cpf;

	@Column(name = "fun_nome", length = 40, nullable = false)
	private String nome;

	@Column(name = "fun_rg", columnDefinition = "char(2)")
	private String rg;

	@Column(name = "fun_rg_orgao_expedidor", length = 20)
	private String rgOrgaoExpedidor;

	@Column(name = "fun_rg_uf", columnDefinition = "char(2)")
	private String rgUf;

	@ElementCollection
	@CollectionTable(name = "tab_telefones_funcionario", 
		joinColumns = @JoinColumn(name = "ftl_fun_cpf", columnDefinition = "char(11)",
		referencedColumnName="fun_cpf", foreignKey = @ForeignKey(name = "fk_ftl_funcionario")))
	@Column(name = "ftl_telefone", length = 12)
	private List<String> telefones;

	@Temporal(TemporalType.DATE)
	@Column(name = "fun_data_nascimento", nullable = false)
	private Date dataNascimento;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "logradouro", column = @Column(name="fun_end_logradouro")),
		@AttributeOverride(name = "bairro", column = @Column(name="fun_end_bairro"))
	})
	@AssociationOverrides({
		@AssociationOverride(name = "cidade", joinColumns = @JoinColumn(name="fun_end_cid_sigla"),
				foreignKey = @ForeignKey(name = "fk_fun_end_cidade"))
	})
	@Column(nullable = false)
	private Endereco endereco;

	public Funcionario() {
		super();
	}

	public Funcionario(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereco) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.rg = rg;
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
		this.rgUf = rgUf;
		this.telefones = telefones;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}
	
	public void addTelefone(String telefone) {
		this.telefones.add(telefone);
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
