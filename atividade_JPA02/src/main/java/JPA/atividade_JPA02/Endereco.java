package JPA.atividade_JPA02;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class Endereco {

	@Column(name = "end_logradouro", length = 10, nullable = false)
	private String logradouro;

	@Column(name = "end_bairro", length = 40, nullable = false)
	private String bairro;

	@ManyToOne
	@JoinColumn(name="end_cid_sigla", referencedColumnName = "cid_sigla", columnDefinition = "char(3)", nullable = false,
		foreignKey = @ForeignKey(name = "fk_end_cidade"))
	private Cidade cidade;

	public Endereco() {
		super();
	}
	
	public Endereco(String logradouro, String bairro, Cidade cidade) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
