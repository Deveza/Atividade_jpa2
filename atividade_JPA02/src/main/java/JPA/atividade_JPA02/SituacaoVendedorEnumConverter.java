package JPA.atividade_JPA02;

import javax.persistence.AttributeConverter;

public class SituacaoVendedorEnumConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	public String convertToDatabaseColumn(SituacaoVendedorEnum situacao) {
		return situacao.getSigla();
	}

	public SituacaoVendedorEnum convertToEntityAttribute(String situacao) {
		return SituacaoVendedorEnum.valueOf(situacao);
	}

}
