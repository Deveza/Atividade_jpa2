package JPA.atividade_JPA02;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name = "adm_fun_cpf", 
	referencedColumnName = "fun_cpf", foreignKey = @ForeignKey(name = "fk_adm_funcionario"), 
	columnDefinition = "char(11)")
@Table(name = "tab_funcionarios_administrativos")
public class Administrativo extends Funcionario {

	@Column(name = "adm_turno", nullable = false)
	private Integer turno;

	public Administrativo() {
		super();
	}

	public Administrativo(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf,
			List<String> telefones, Date dataNascimento, Endereco endereco, Integer turno) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereco);
		this.turno = turno;
	}

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}
}
