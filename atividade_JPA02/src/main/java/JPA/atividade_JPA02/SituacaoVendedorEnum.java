package JPA.atividade_JPA02;

public enum SituacaoVendedorEnum {
	ATIVO("ATV"), SUSPENSO("SPN");
	
	private String sigla;
	
	private SituacaoVendedorEnum(String sigla) {
		this.sigla = sigla;
	}

	public String getSigla() {
		return sigla;
	}

	public static SituacaoVendedorEnum ValueOfSigla(String sigla) {
		for(SituacaoVendedorEnum situacao : values()) {
				if(situacao.getSigla().equalsIgnoreCase(sigla)) 
					return situacao;
		}
		throw new IllegalArgumentException();
	}
	
}
