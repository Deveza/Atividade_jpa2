package JPA.atividade_JPA02;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceJpa {

	public static void main(String[] args) {

		// https://www.objectdb.com/java/jpa

		EntityManagerFactory factory = null;

		try {

			factory = Persistence.createEntityManagerFactory("exercicio-jpa02-pu");
			EntityManager em = factory.createEntityManager();

			em.getTransaction().begin();

			em.getTransaction().commit();

		} finally {

			if (factory != null) {
				factory.close();
			}

		}

	}

}
