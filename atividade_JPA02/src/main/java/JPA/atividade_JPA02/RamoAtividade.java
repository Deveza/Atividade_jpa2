package JPA.atividade_JPA02;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_ramo_atividade")
public class RamoAtividade {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ramo_atividade")
	@SequenceGenerator(name = "seq_ramo_atividade", sequenceName="seq_ramo_atividade", allocationSize = 100)
	@Column(name = "rma_id")
	private Integer id;
	
	@Column(name = "rma_nome", length = 40, nullable = false)
	private String nome;
	
	public RamoAtividade() {
		super();
	}
	
	public RamoAtividade(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
